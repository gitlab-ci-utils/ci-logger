'use strict';

/**
 * A collection of type-checking functions. Each throws if the
 * expected type is not found.
 *
 * @module assert
 */

/**
 * Throws a common TypeError indicating the expected and actual types.
 *
 * @param  {string}    expectedType The expected type for the value.
 * @param  {string}    valueType    The type of the value being checked.
 * @throws {TypeError}              Always throws.
 * @private
 */
const throwTypeError = (expectedType, valueType) => {
    throw new TypeError(
        `Expected value of type "${expectedType}" but received "${valueType}"`
    );
};

/**
 * Throws if the given value is not an object.
 *
 * @param  {object}    value The value to be checked.
 * @throws {TypeError}       Throws if value is not a valid object.
 * @static
 * @public
 */
const object = (value) => {
    if (value === null) {
        throwTypeError('object', 'null');
    }
    if (typeof value !== 'object') {
        throwTypeError('object', typeof value);
    }
};

/**
 * Throws if the given value is not a string.
 *
 * @param  {string}    value The value to be checked.
 * @throws {TypeError}       Throws if value is not a valid string.
 * @static
 * @public
 */
const string = (value) => {
    if (typeof value !== 'string') {
        throwTypeError('string', typeof value);
    }
};

/**
 * Throws if the given value is not an integer.
 *
 * @param  {integer}   value The value to be checked.
 * @throws {TypeError}       Throws if value is not a valid integer.
 * @static
 * @public
 */
const integer = (value) => {
    if (!Number.isInteger(value)) {
        throwTypeError('integer', typeof value);
    }
};

/**
 * Throws if the given value is not a boolean.
 *
 * @param  {boolean}   value The value to be checked.
 * @throws {TypeError}       Throws if value is not a valid boolean.
 * @static
 * @public
 */
const boolean = (value) => {
    if (value !== true && value !== false) {
        throwTypeError('boolean', typeof value);
    }
};

module.exports = { boolean, integer, object, string };
