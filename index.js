'use strict';

/**
 * CI Logger is a very simple logger designed for CI environments. Log entries
 * can be formatted to indicate the results of a previous message, and the
 * process can be terminated of an error is logged.
 *
 * @module ci-logger
 */

const assert = require('./lib/assert');

/**
 * Enum for log level.
 *
 * @readonly
 * @enum {string}
 * @static
 */
const Levels = Object.freeze({
    Error: 'error',
    Info: 'info',
    Warn: 'warn'
});

const getOriginalLogEntryDefaults = () =>
    Object.freeze({
        errorCode: 1,
        exitOnError: true,
        isResult: false,
        level: Levels.Info,
        resultPrefix: '\u2BA1'
    });

let logEntryDefaults = getOriginalLogEntryDefaults();

const validateAndAddSetting = (entry, property, type, result) => {
    // A setting could allow a falsy value, so check for the existence of the property
    if (Object.keys(entry).includes(property)) {
        // Call the specified assert type function, which will throw on type error.
        // All values hard-coded in this module.
        // nosemgrep: eslint.detect-object-injection, unsafe-dynamic-method
        assert[type](entry[property]);
        // All values hard-coded in this module.
        // nosemgrep: eslint.detect-object-injection
        result[property] = entry[property];
    }
};

const validateLogSettings = (entry) => {
    const result = {};

    validateAndAddSetting(entry, 'errorCode', 'integer', result);
    validateAndAddSetting(entry, 'exitOnError', 'boolean', result);
    validateAndAddSetting(entry, 'isResult', 'boolean', result);
    validateAndAddSetting(entry, 'resultPrefix', 'string', result);

    if (Object.keys(entry).includes('level')) {
        if (!Object.values(Levels).includes(entry.level)) {
            throw new TypeError(
                `Level must be a valid value (${Object.values(Levels).join(
                    ', '
                )})`
            );
        }
        result.level = entry.level;
    }

    return Object.assign(Object.create(null), logEntryDefaults, result);
};

/**
 * Sets log entry default values, which will be used in any subsequent log calls.
 *
 * @param {object}  settings                An object containing any valid log entry settings.
 * @param {Levels}  [settings.level]        The log level for the entry.
 * @param {boolean} [settings.isResult]     Indicates the log entry is a result of another action,
 *                                          and prefixes the entry with resultPrefix.
 * @param {string}  [settings.resultPrefix] String prefixing the message if isResult is true.
 * @param {boolean} [settings.exitOnError]  If true, exits the process with the specified
 *                                          errorCode if level is error.
 * @param {number}  [settings.errorCode]    The error code used if the process exits.
 * @static
 */
const setLogEntryDefaults = (settings) => {
    logEntryDefaults = validateLogSettings(settings);
};

/**
 * Resets the log entry defaults to the original values.
 *
 * @static
 */
const resetLogEntryDefaults = () => {
    logEntryDefaults = getOriginalLogEntryDefaults();
};

/**
 * Validates the supplied log entry values and returns a complete log entry, including the
 * current default values.
 *
 * @param   {object}  entry                An object with log entry.
 * @param   {string}  entry.message        The message to be logged.
 * @param   {Levels}  [entry.level]        The log level for the entry.
 * @param   {boolean} [entry.isResult]     Indicates the log entry is a result of another action,
 *                                         and prefixes the entry with resultPrefix.
 * @param   {string}  [entry.resultPrefix] String prefixing the message if isResult is true.
 * @param   {boolean} [entry.exitOnError]  If true, exits the process with the specified
 *                                         errorCode if level is error.
 * @param   {number}  [entry.errorCode]    The error code used if the process exits.
 * @returns {object}                       A complete log entry object including the current
 *                                         default values.
 * @static
 */
const getLogEntry = (entry) => {
    assert.object(entry);
    assert.string(entry.message);

    return {
        ...validateLogSettings(entry),
        message: entry.message
    };
};

const getLogMethod = (level) => {
    let logMethod;
    switch (level) {
        case Levels.Warn: {
            logMethod = console.warn;
            break;
        }
        case Levels.Error: {
            logMethod = console.error;
            break;
        }
        default: {
            logMethod = console.log;
            break;
        }
    }
    return logMethod;
};

/**
 * Logs the given log entry (to stdout or stderr based on entry level). Any log entry
 * properties not specified will use the current default value. If the the level is
 * error and exitOnError is true, exits the process with the specified exitCode.
 *
 * @param {object}  entry                An object with log entry.
 * @param {string}  entry.message        The message to be logged.
 * @param {Levels}  [entry.level]        The log level for the entry.
 * @param {boolean} [entry.isResult]     Indicates the log entry is a result of another action,
 *                                       and prefixes the entry with resultPrefix.
 * @param {string}  [entry.resultPrefix] String prefixing the message if isResult is true.
 * @param {boolean} [entry.exitOnError]  If true, exits the process with the specified
 *                                       errorCode if level is error.
 * @param {number}  [entry.errorCode]    The error code used if the process exits.
 * @static
 */
const log = (entry) => {
    const logEntry = getLogEntry(entry);
    const logMethod = getLogMethod(logEntry.level);

    const { message, isResult, resultPrefix, level, exitOnError, errorCode } =
        logEntry;
    logMethod(isResult ? ` ${resultPrefix} ${message}` : message);

    if (level === Levels.Error && exitOnError) {
        console.error(`Fatal error - exiting (${errorCode})`);
        /* eslint-disable-next-line unicorn/no-process-exit -- want process to
           exit with appropriate error code */
        process.exit(errorCode);
    }
};

module.exports.Levels = Levels;
module.exports.log = log;
module.exports.getLogEntry = getLogEntry;
module.exports.resetLogEntryDefaults = resetLogEntryDefaults;
module.exports.setLogEntryDefaults = setLogEntryDefaults;
