# CI Logger

CI Logger is a simple logger designed for CI environments - no colors, no
timestamps - just data. Log entries can be formatted to indicate the results
of a previous message, and the process can be terminated of an error is logged.

## Usage

```js
const { log, Levels } = require('ci-logger');

log({ message: 'Retrieving data from somewhere...' });
// Retrieving data from somewhere...

log({
  message: 'Error retrieving data',
  isResult: true,
  level: 'warn'
});
//  ⮡ Error retrieving data

log({
  message: 'Error retrieving data',
  isResult: true,
  level: Levels.Error,
  exitOnError: true,
  errorCode: 2
});
//  ⮡ Error retrieving data
// Fatal error - exiting (2)
```

The `log` function must be passed an object with the following possible
properties:

- The `message` property contains the message to be logged and is the only
  required property. It can be any value except `undefined` or `null`.
- The `level` property must be one of the strings `info`, `warn`, or `error`
  (which are the values of the `Levels` enumeration exposed by the module -
  `Levels.Info`, `Levels.Warn`, `Levels.Error`). The default value is `info`
  (`Levels.Info`).
- The `isResult` property is a Boolean value intended to indicate the result of
  an operation, primarily intended to simplify reading busy CI console logs. If
  `isResult` is true, the logged message is indented and prefixed with the
  `resultPrefix` string value to indicate it's the result of the preceding
  message. If false, the message isn't altered. The default value of `isResult`
  is `false`. The default value for `resultPrefix` is '\u2BA1' ('⮡ ').
- The `exitOnError` property is a Boolean value indicating whether the process
  should exit if an error is logged. If true, the error is logged, a fatal
  error message is logged, and `process.exit` is called with the `errorCode`
  value (an integer). The default value of `exitOnError` is true, and the
  default value of `errorCode` is 1.

The `getLogEntry` function can be used to retrieve a complete log entry as it
would be logged, with default values populating any unspecified values.

```js
const { getLogEntry } = require('ci-logger');
const logEntry = getLogEntry({
  message: 'Retrieving data from somewhere...'
});
// logEntry = {
//     message: 'Retrieving data from somewhere...',
//     isResult: false,
//     level: 'info',
//     exitOnError: true,
//     errorCode: 1,
//     resultPrefix: '\u2BA1'
// }
```

## Default log entry values

The default values for `errorCode`, `exitOnError`, `isResult`, `level`, and
`resultPrefix`, as detailed previously, are used when they're not specified for
a specific log entry. If desired, those defaults can be changed using the
`setLogEntryDefaults` function. This function accepts an object with any or all
of these values.

```js
const { setLogEntryDefaults, Levels } = require('ci-logger');
setLogEntryDefaults({ errorCode: 2, level: Levels.Warn });
```

The default values can be set back to the original values via the
`resetLogEntryDefaults` function.

```js
const { resetLogEntryDefaults } = require('ci-logger');
resetLogEntryDefaults();
```
