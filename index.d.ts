/**
 * Log entry settings
 */
export interface LogEntrySettings {
    /**
     * The log level.
     *
     * @default 'info'
     */
    level?: 'info' | 'error' | 'warn';
    /**
     * Indicates the log entry is a result of another action,
     * and prefixes the entry with resultPrefix.
     *
     * @default false
     */
    isResult?: boolean;
    /**
     * String prefixing the message if isResult is true.
     *
     * @default '\u2BA1'
     */
    resultPrefix?: string;
    /**
     * If true, exits the process with the specified
     * errorCode if level is error.
     *
     * @default true
     */
    exitOnError?: boolean;
    /**
     * The error code used if the process exits.
     *
     * @default 1
     */
    errorCode?: number;
}

/**
 * Log entry
 */
export interface LogEntry extends LogEntrySettings {
    /**
     * The message to be logged.
     */
    message: string;
}

/**
 * Logs the given log entry (to stdout or stderr based on entry level). Any log entry
 * properties not specified will use the current default value. If the the level is
 * error and exitOnError is true, exits the process with the specified exitCode.
 *
 * @static
 * @param  entry   A LogEntry object.
 */
export function log(entry: LogEntry): void;

/**
 * Validates the supplied log entry values and returns a complete log entry, including the
 * current default values.
 *
 * @static
 * @param  entry   A LogEntry object.
 */
export function getLogEntry(entry: LogEntry): LogEntry;

/**
 * Resets the log entry defaults to the original values.
 *
 * @static
 */
export function resetLogEntryDefaults(): void;

/**
 * Sets log entry default values, which will be used in any subsequent log calls.
 *
 * @static
 * @param  settings  A LogEntrySettings object.
 */
export function setLogEntryDefaults(settings: LogEntrySettings): void;
