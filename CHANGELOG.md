# Changelog

## v7.0.0 (2024-04-29)

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#43)

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#42)

## v6.0.0 (2023-06-02)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#37)

## v5.1.1 (2023-02-05)

### Fixed

- Refactored code for changes in latest `eslint` rules.

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared `medium` sized
  runners. (#34)

## v5.1.0 (2022-08-26)

### Changed

- Replaced `@sindresorhus/is` with custom type-checking module to eliminate all production dependencies. (#33)

## v5.0.0 (2022-06-05)

### Changed

- BREAKING: Capitalized the `levels` enumeration (now `Levels`) and values to better conform to convention. The values are now `Levels.Info`, `Levels.Warn`, `Levels.Error`. These must be updated in any place this enumeration is used or a `TypeError` will be thrown. The actual string values are still the same (`info`, `warn`, `error`), which is what log levels are validated against, so if those are used no changes are required. (#32)
- BREAKING: Added TypeScript type declarations. (#24)
- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#28, #29)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added test coverage threshold requirements (#31)

## v4.0.2 (2022-04-24)

### Miscellaneous

- Updated to latest dependencies, including resolving CVE-2021-44906 (dev only)
- Refactored code to comply with latest `eslint` config
- Added `prettier` and disabled `eslint` formatting rules. (#27)

## v4.0.1 (2021-08-29)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Configured [renovate](https://docs.renovatebot.com/) for dependency updates (#15)

## v4.0.0 (2021-06-01)

### Changed

- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#22)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v3.0.5 (2021-05-09)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Optimize published package to only include the minimum required files (#21)

## v3.0.4 (2021-03-14)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated pipeline to use standard NPM package collection (#20)

## v3.0.3 (2021-02-21)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v3.0.2 (2020-11-25)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated documentation for all exposed functions for consistency (#17)
- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#18) and GitLab Releaser (#19)

## v3.0.1 (2020-08-30)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v3.0.0 (2020-06-28)

### Added

- Added capability to set defaults and reset them to the original values (#2)

### Changed

- BREAKING: Added explicit type checking for all log entry values. Will throw for an invalid value. Previously invalid values were ignored and replaced with default values. (#16)
- BREAKING: Removed Node v13 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=14.0.0`) (#14)
- Add log entry and default setting for resultPrefix (#15)

### Fixed

- Updated to latest dependencies
- Updated to latest GitLab `license_scanning` and `secret_detection` jobs

## v2.0.1 (2020-03-22)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v2.0.0 (2020-01-12)

### Changed

- BREAKING: Removed Node v8 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=13.0.0`) (#12)
- Updated the exit on error message to include the exit code (#13)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added test jobs for other all supported NodeJS versions (#10)
- Updated project to use [eslint-plugin-sonarjs](https://www.npmjs.com/package/eslint-plugin-sonarjs) (#11)

## v1.0.3 (2019-11-24)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.0.2 (2019-11-17)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

### Miscellaneous

- Updated project to use custom eslint configuration module (#9)

## v1.0.1 (2019-10-23)

### Fixed

- Updated to latest dependencies

## v1.0.0 (2019-06-16)

Initial release
