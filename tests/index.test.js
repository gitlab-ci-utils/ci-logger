'use strict';

const {
    getLogEntry,
    log,
    resetLogEntryDefaults,
    setLogEntryDefaults,
    Levels
} = require('../index');

const message = 'something';

const originalLogEntryDefaults = Object.freeze({
    errorCode: 1,
    exitOnError: true,
    isResult: false,
    level: Levels.Info,
    resultPrefix: '\u2BA1'
});

describe('get log entry', () => {
    it('should throw if invalid args provided', () => {
        expect.assertions(1);
        expect(() => getLogEntry()).toThrow(/undefined/);
    });

    it('should throw if args has invalid message', () => {
        expect.assertions(1);
        expect(() => getLogEntry({})).toThrow(/string/);
    });

    it('should populate logEntry with message', () => {
        expect.assertions(1);
        const logInput = { message };
        const logEntry = getLogEntry(logInput);
        expect(logEntry.message).toBe(logInput.message);
    });

    it('should not throw if args has valid message', () => {
        expect.assertions(1);
        const logInput = { message };
        expect(() => getLogEntry(logInput)).not.toThrow();
    });

    it('should populate logEntry with errorCode if provided', () => {
        expect.assertions(1);
        const logInput = {
            errorCode: originalLogEntryDefaults.errorCode + 1,
            message
        };
        const logEntry = getLogEntry(logInput);
        expect(logEntry.errorCode).toBe(logInput.errorCode);
    });

    it('should populate logEntry with default errorCode if not provided', () => {
        expect.assertions(1);
        const logInput = { message };
        const logEntry = getLogEntry(logInput);
        expect(logEntry.errorCode).toBe(originalLogEntryDefaults.errorCode);
    });

    it('should throw if invalid errorCode is provided', () => {
        expect.assertions(1);
        const logInput = { errorCode: '3', message };
        expect(() => getLogEntry(logInput)).toThrow('integer');
    });

    it('should populate logEntry with Level enum if provided and valid', () => {
        expect.assertions(3);
        const logInput = { level: Levels.Info, message };
        let logEntry = getLogEntry(logInput);
        expect(logEntry.level).toBe(logInput.level);

        logInput.level = Levels.Warn;
        logEntry = getLogEntry(logInput);
        expect(logEntry.level).toBe(logInput.level);

        logInput.level = Levels.Error;
        logEntry = getLogEntry(logInput);
        expect(logEntry.level).toBe(logInput.level);
    });

    it('should populate logEntry with level string if provided and valid', () => {
        expect.assertions(3);
        const logInput = { level: 'info', message };
        let logEntry = getLogEntry(logInput);
        expect(logEntry.level).toBe('info');

        logInput.level = 'warn';
        logEntry = getLogEntry(logInput);
        expect(logEntry.level).toBe('warn');

        logInput.level = 'error';
        logEntry = getLogEntry(logInput);
        expect(logEntry.level).toBe('error');
    });

    it('should populate logEntry with default level if not provided', () => {
        expect.assertions(1);
        const logInput = { message };
        const logEntry = getLogEntry(logInput);
        expect(logEntry.level).toBe(originalLogEntryDefaults.level);
    });

    it('should throw if invalid level provided', () => {
        expect.assertions(1);
        const logInput = { level: 'debug', message };
        expect(() => getLogEntry(logInput)).toThrow('valid value');
    });

    it('should populate logEntry with exitOnError if true/false provided', () => {
        expect.assertions(2);
        const logInput = { exitOnError: true, message };
        let logEntry = getLogEntry(logInput);
        expect(logEntry.exitOnError).toBe(logInput.exitOnError);

        logInput.exitOnError = false;
        logEntry = getLogEntry(logInput);
        expect(logEntry.exitOnError).toBe(logInput.exitOnError);
    });

    it('should populate logEntry with default exitOnError if not provided', () => {
        expect.assertions(1);
        const logInput = { message };
        const logEntry = getLogEntry(logInput);
        expect(logEntry.exitOnError).toBe(originalLogEntryDefaults.exitOnError);
    });

    it('should throw if invalid exitOnError value provided', () => {
        expect.assertions(1);
        const logInput = { exitOnError: 'blue', message };
        expect(() => getLogEntry(logInput)).toThrow('boolean');
    });

    it('should populate logEntry with isResult if true/false provided', () => {
        expect.assertions(2);
        const logInput = { isResult: true, message };
        let logEntry = getLogEntry(logInput);
        expect(logEntry.isResult).toBe(logInput.isResult);

        logInput.isResult = false;
        logEntry = getLogEntry(logInput);
        expect(logEntry.isResult).toBe(logInput.isResult);
    });

    it('should populate logEntry with default isResult if not provided', () => {
        expect.assertions(1);
        const logInput = { message };
        const logEntry = getLogEntry(logInput);
        expect(logEntry.isResult).toBe(originalLogEntryDefaults.isResult);
    });

    it('should throw if invalid isResults value provided', () => {
        expect.assertions(1);
        const logInput = { isResult: 'blue', message };
        expect(() => getLogEntry(logInput)).toThrow('boolean');
    });

    it('should populate logEntry with resultPrefix if provided', () => {
        expect.assertions(2);
        const logInput = { message, resultPrefix: '-' };
        let logEntry = getLogEntry(logInput);
        expect(logEntry.resultPrefix).toBe(logInput.resultPrefix);

        logInput.resultPrefix = 'foo';
        logEntry = getLogEntry(logInput);
        expect(logEntry.resultPrefix).toBe(logInput.resultPrefix);
    });

    it('should populate logEntry with default resultPrefix if not provided', () => {
        expect.assertions(1);
        const logInput = { message };
        const logEntry = getLogEntry(logInput);
        expect(logEntry.resultPrefix).toBe(
            originalLogEntryDefaults.resultPrefix
        );
    });

    it('should throw if invalid resultPrefix value provided', () => {
        expect.assertions(1);
        const logInput = { message, resultPrefix: true };
        expect(() => getLogEntry(logInput)).toThrow('boolean');
    });
});

describe('log', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    const logWithSpies = (logEntry) => {
        const consoleLogSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        const consoleWarnSpy = jest
            .spyOn(console, 'warn')
            .mockImplementation(() => {});
        const consoleErrorSpy = jest
            .spyOn(console, 'error')
            .mockImplementation(() => {});
        const processExitSpy = jest
            .spyOn(process, 'exit')
            .mockImplementation(() => {});

        log(logEntry);

        return {
            consoleErrorSpy,
            consoleLogSpy,
            consoleWarnSpy,
            processExitSpy
        };
    };

    it('should call console.log with message if Levels.Info', () => {
        expect.assertions(1);
        const logEntry = { level: Levels.Info, message };
        const result = logWithSpies(logEntry);
        /* eslint-disable-next-line jest/prefer-called-with -- log value checked elsewhere */
        expect(result.consoleLogSpy).toHaveBeenCalled();
    });

    it('should call console.warn with message if Levels.Warn', () => {
        expect.assertions(1);
        const logEntry = { level: Levels.Warn, message };
        const result = logWithSpies(logEntry);
        /* eslint-disable-next-line jest/prefer-called-with -- log value checked elsewhere */
        expect(result.consoleWarnSpy).toHaveBeenCalled();
    });

    it('should call console.error with message and exit with error code if Levels.Error and exitOnError is true', () => {
        expect.assertions(4);
        const logEntry = {
            errorCode: 123,
            exitOnError: true,
            level: Levels.Error,
            message
        };
        const result = logWithSpies(logEntry);
        // Console.error should be called two times in this case
        const errorCalls = 2;
        expect(result.consoleErrorSpy).toHaveBeenCalledTimes(errorCalls);
        // The first call should be with the log message
        expect(result.consoleErrorSpy).toHaveBeenNthCalledWith(
            1,
            logEntry.message
        );
        // The second call should be with the exit message with error code
        expect(result.consoleErrorSpy).toHaveBeenNthCalledWith(
            errorCalls,
            expect.stringContaining(String(logEntry.errorCode))
        );
        expect(result.processExitSpy).toHaveBeenCalledWith(logEntry.errorCode);
    });

    it('should not exit if Levels.Error and exitOnError is false', () => {
        expect.assertions(2);
        const logEntry = {
            exitOnError: false,
            level: Levels.Error,
            message
        };
        const result = logWithSpies(logEntry);
        /* eslint-disable-next-line jest/prefer-called-with -- log value checked elsewhere */
        expect(result.consoleErrorSpy).toHaveBeenCalled();
        expect(result.processExitSpy).not.toHaveBeenCalled();
    });

    it('should not exit if not Levels.Error and exitOnError is true', () => {
        expect.assertions(2);
        const logEntry = {
            exitOnError: true,
            level: Levels.Warn,
            message
        };
        const result = logWithSpies(logEntry);
        /* eslint-disable-next-line jest/prefer-called-with -- log value checked elsewhere */
        expect(result.consoleWarnSpy).toHaveBeenCalled();
        expect(result.processExitSpy).not.toHaveBeenCalled();
    });

    it('should format message if isResult is true', () => {
        expect.assertions(1);
        const logEntry = { isResult: true, message };
        const result = logWithSpies(logEntry);

        const loggedMessage = result.consoleLogSpy.mock.calls[0][0];
        expect(loggedMessage).toBe(
            ` ${originalLogEntryDefaults.resultPrefix} ${logEntry.message}`
        );
    });

    it('should format message with resultPrefix if isResult is true and resultPrefix provided', () => {
        expect.assertions(1);
        const logEntry = { isResult: true, message, resultPrefix: '-' };
        const result = logWithSpies(logEntry);

        const loggedMessage = result.consoleLogSpy.mock.calls[0][0];
        expect(loggedMessage).toBe(
            ` ${logEntry.resultPrefix} ${logEntry.message}`
        );
    });

    it('should not format message if isResult is false', () => {
        expect.assertions(1);
        const logEntry = { isResult: false, message };
        const result = logWithSpies(logEntry);

        const loggedMessage = result.consoleLogSpy.mock.calls[0][0];
        expect(loggedMessage).toBe(logEntry.message);
    });
});

describe('resetLogEntryDefaults', () => {
    afterEach(() => {
        resetLogEntryDefaults();
    });

    it('should return log entry with original defaults after reset', () => {
        expect.assertions(3);
        // Get original log entry with default values for comparison
        const logEntryWithOriginalDefaults = getLogEntry({ message });
        expect(logEntryWithOriginalDefaults).toMatchSnapshot('default values');

        // Modify all settings to ensure that all are reset
        const modifiedLogEntryDefaults = {
            errorCode: 2,
            exitOnError: false,
            isResult: true,
            level: Levels.Warn,
            resultPrefix: '-'
        };
        setLogEntryDefaults(modifiedLogEntryDefaults);
        expect(getLogEntry({ message })).toMatchSnapshot(
            'after setting defaults'
        );

        resetLogEntryDefaults();
        expect(getLogEntry({ message })).toStrictEqual(
            logEntryWithOriginalDefaults
        );
    });
});

describe('setLogEntryDefaults', () => {
    const testSetLogEntryDefaults = (newDefaults) => {
        setLogEntryDefaults(newDefaults);
        const logEntry = { message };
        const expectedResults = {
            ...originalLogEntryDefaults,
            ...newDefaults,
            ...logEntry
        };
        expect(getLogEntry(logEntry)).toStrictEqual(expectedResults);
    };

    afterEach(() => {
        resetLogEntryDefaults();
    });

    it('should update default errorCode if specified', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { errorCode: 3 };
        testSetLogEntryDefaults(updatedLogEntryDefaults);
    });

    it('should throw if default errorCode is not an integer', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { errorCode: 'foo' };
        expect(() => setLogEntryDefaults(updatedLogEntryDefaults)).toThrow(
            'integer'
        );
    });

    it('should update default exitOnError if specified', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { exitOnError: false };
        testSetLogEntryDefaults(updatedLogEntryDefaults);
    });

    it('should throw if default exitOnError is not boolean', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { exitOnError: 0 };
        expect(() => setLogEntryDefaults(updatedLogEntryDefaults)).toThrow(
            'boolean'
        );
    });

    it('should update default isResult if specified', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { isResult: true };
        testSetLogEntryDefaults(updatedLogEntryDefaults);
    });

    it('should throw if default isResult is not boolean', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { isResult: 0 };
        expect(() => setLogEntryDefaults(updatedLogEntryDefaults)).toThrow(
            'boolean'
        );
    });

    it('should update default level if specified', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { level: Levels.Warn };
        testSetLogEntryDefaults(updatedLogEntryDefaults);
    });

    it('should throw if default level is not valid', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { level: 'foo' };
        expect(() => setLogEntryDefaults(updatedLogEntryDefaults)).toThrow(
            'valid value'
        );
    });

    it('should update default resultPrefix if specified', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { resultPrefix: '-' };
        testSetLogEntryDefaults(updatedLogEntryDefaults);
    });

    it('should throw if default resultPrefix is not valid', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = { resultPrefix: 1 };
        expect(() => setLogEntryDefaults(updatedLogEntryDefaults)).toThrow(
            'string'
        );
    });

    it('should update all defaults if specified', () => {
        expect.assertions(1);
        const updatedLogEntryDefaults = {
            errorCode: 2,
            exitOnError: false,
            isResult: true,
            level: Levels.Warn,
            resultPrefix: '-'
        };
        testSetLogEntryDefaults(updatedLogEntryDefaults);
    });
});
