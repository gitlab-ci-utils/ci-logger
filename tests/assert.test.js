'use strict';

const assert = require('../lib/assert');

const testValues = {
    boolean: false,
    float: 3.14,
    integer: 123,
    object: { foo: 'bar' },
    string: 'foo'
};

describe('assert', () => {
    it('should expose an object function', () => {
        expect.assertions(1);
        expect(typeof assert.object).toBe('function');
    });

    it('should expose a string function', () => {
        expect.assertions(1);
        expect(typeof assert.string).toBe('function');
    });

    it('should expose an integer function', () => {
        expect.assertions(1);
        expect(typeof assert.integer).toBe('function');
    });

    it('should expose a boolean function', () => {
        expect.assertions(1);
        expect(typeof assert.boolean).toBe('function');
    });

    describe('object', () => {
        it('should not throw if value is a valid object', () => {
            expect.assertions(1);
            expect(() => assert.object(testValues.object)).not.toThrow();
        });

        it('should throw if value is undefined', () => {
            expect.assertions(1);
            expect(() => assert.object()).toThrow(
                'Expected value of type "object" but received "undefined"'
            );
        });

        it('should throw if value is not an object (null)', () => {
            expect.assertions(1);
            // eslint-disable-next-line unicorn/no-null -- testing null
            expect(() => assert.object(null)).toThrow(
                'Expected value of type "object" but received "null"'
            );
        });

        it('should throw if value is not an object (string)', () => {
            expect.assertions(1);
            expect(() => assert.object(testValues.string)).toThrow(
                'Expected value of type "object" but received "string"'
            );
        });

        it('should throw if value is not an object (integer)', () => {
            expect.assertions(1);
            expect(() => assert.object(testValues.integer)).toThrow(
                'Expected value of type "object" but received "number"'
            );
        });

        it('should throw if value is not an object (boolean)', () => {
            expect.assertions(1);
            expect(() => assert.object(testValues.boolean)).toThrow(
                'Expected value of type "object" but received "boolean"'
            );
        });
    });

    describe('string', () => {
        it('should not throw if value is a valid string', () => {
            expect.assertions(1);
            expect(() => assert.string(testValues.string)).not.toThrow();
        });

        it('should throw if value is undefined', () => {
            expect.assertions(1);
            expect(() => assert.string()).toThrow(
                'Expected value of type "string" but received "undefined"'
            );
        });

        it('should throw if value is not a string (object)', () => {
            expect.assertions(1);
            expect(() => assert.string(testValues.object)).toThrow(
                'Expected value of type "string" but received "object"'
            );
        });

        it('should throw if value is not a string (integer)', () => {
            expect.assertions(1);
            expect(() => assert.string(testValues.integer)).toThrow(
                'Expected value of type "string" but received "number"'
            );
        });

        it('should throw if value is not a string (boolean)', () => {
            expect.assertions(1);
            expect(() => assert.string(testValues.boolean)).toThrow(
                'Expected value of type "string" but received "boolean"'
            );
        });
    });

    describe('integer', () => {
        it('should not throw if value is a valid integer', () => {
            expect.assertions(1);
            expect(() => assert.integer(testValues.integer)).not.toThrow();
        });

        it('should throw if value is undefined', () => {
            expect.assertions(1);
            expect(() => assert.integer()).toThrow(
                'Expected value of type "integer" but received "undefined"'
            );
        });

        it('should throw if value is not an integer (float)', () => {
            expect.assertions(1);
            expect(() => assert.integer(testValues.float)).toThrow(
                'Expected value of type "integer" but received "number"'
            );
        });

        it('should throw if value is not an integer (object)', () => {
            expect.assertions(1);
            expect(() => assert.integer(testValues.object)).toThrow(
                'Expected value of type "integer" but received "object"'
            );
        });

        it('should throw if value is not an integer (string)', () => {
            expect.assertions(1);
            expect(() => assert.integer(testValues.string)).toThrow(
                'Expected value of type "integer" but received "string"'
            );
        });

        it('should throw if value is not an integer (boolean)', () => {
            expect.assertions(1);
            expect(() => assert.integer(testValues.boolean)).toThrow(
                'Expected value of type "integer" but received "boolean"'
            );
        });
    });

    describe('boolean', () => {
        it('should not throw if value is a valid boolean', () => {
            expect.assertions(1);
            expect(() => assert.boolean(testValues.boolean)).not.toThrow();
        });

        it('should throw if value is undefined', () => {
            expect.assertions(1);
            expect(() => assert.boolean()).toThrow(
                'Expected value of type "boolean" but received "undefined"'
            );
        });

        it('should throw if value is not a boolean (object)', () => {
            expect.assertions(1);
            expect(() => assert.boolean(testValues.object)).toThrow(
                'Expected value of type "boolean" but received "object"'
            );
        });

        it('should throw if value is not a boolean (string)', () => {
            expect.assertions(1);
            expect(() => assert.boolean(testValues.string)).toThrow(
                'Expected value of type "boolean" but received "string"'
            );
        });

        it('should throw if value is not a boolean (integer)', () => {
            expect.assertions(1);
            expect(() => assert.boolean(testValues.integer)).toThrow(
                'Expected value of type "boolean" but received "number"'
            );
        });
    });
});
