import { expectError, expectType } from 'tsd';
import {
    LogEntry,
    getLogEntry,
    log,
    setLogEntryDefaults,
    LogEntrySettings
} from '.';

const message = 'This is a test';

// Test creation of all interface types
const settings: LogEntrySettings = {
    errorCode: 1,
    exitOnError: true,
    isResult: true,
    level: 'warn',
    resultPrefix: ' - '
};

const entry: LogEntry = {
    message,
    ...settings
};

const settingCases: LogEntrySettings[] = [
    { errorCode: 2 },
    { exitOnError: false },
    { isResult: true },
    { level: 'warn' },
    { resultPrefix: '-' }
];

// Test each function without required property
expectError(getLogEntry({}));
expectError(log({}));

// Test each function with required property
getLogEntry({ message });
getLogEntry(entry);
log({ message });
log(entry);

// Test each function with individual setting
for (const settingCase of settingCases) {
    getLogEntry({ message, ...settingCase });
    log({ message, ...settingCase });
    setLogEntryDefaults(settingCase);
}

// Test return types for each function
expectType<LogEntry>(getLogEntry({ message: 'foo' }));
